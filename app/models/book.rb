class Book < ApplicationRecord
belongs_to :author
has_many :categorizations
  has_many :categories, through: :categorizations 
belongs_to :publisher
has_many :order_items

def self.search(search)
  if search
    where( "book_name LIKE ? ", "%#{search}%")
  else
    all
  end
end

def self.a_search(a_id)
  if a_id
  	where( author_id: "%#{a_id}%")
    
  else
    all
  end
end

end

