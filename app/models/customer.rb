class Customer < ApplicationRecord
has_many :authorization_orders

#attr_accessible :email, :password, :password_confirmation

  
  attr_accessor :password
  attr_reader :password_hash
  #before_save :encrypt_password
  
  validates_confirmation_of :password
  validates_presence_of :password, :on => :create
  validates_presence_of :email
  validates_uniqueness_of	 :email
  
  def self.authenticate(email, password)
    customer = find_by_email(email)
    if customer #&& customer.password_hash == BCrypt::Engine.hash_secret(password, customer.password_salt)
      customer
    else
      nil
    end
  end
  

end

#end
#class Customer < ActiveRecord::Base
#validates :name, presence: true
#end