class OrderItem < ApplicationRecord
belongs_to :book
belongs_to :authorization_order
belongs_to :no_authorization_order
end
