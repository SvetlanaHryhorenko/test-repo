class Author < ApplicationRecord
has_many :books

def self.search(search)
  if search
   # find( "author_name LIKE ? ", "%#{search}%")
   where( "author_name LIKE ? ", "%#{search}%")

  
  else
    all
  end
end

end
