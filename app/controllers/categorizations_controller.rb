class CategorizationsController < ApplicationController
before_action :set_categorization, only: [:show, :edit, :update, :destroy]

  def index
  # @publisher = Publisher.find(1)
#  @books = @publisher.books.all
    
@categorizations = Categorization.all

#@message = "Hello"
  end
  
  def show
  end
  
  def new
    @categorization = Categorization.new
  end
  
  def edit
  end
  
  def create

    @categorization = Categorization.new(categorization_params)
    respond_to do |format|
      if @categorization.save
       format.html { redirect_to @categorization, notice: ' was successfully created.' }
        format.json { render :show, status: :created, location: @categorization }
      else
        format.html { render :new }
        format.json { render json: @categorization.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    respond_to do |format|
      if @categorization.update(categorization_params)
       # format.html { redirect_to @categorization, notice: ' was successfully updated.' }
       # format.json { render :show, status: :ok, location: @categorization }
      else
       # format.html { render :edit }
        format.json { render json: @categorization.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @categorization.destroy
    respond_to do |format|
      format.html { redirect_to categorizations_url, notice: ' was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_categorization
      @categorization = Categorization.find(params[:id])
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def categorization_params
      params.require(:categorization).permit(:category_id, :book_id)
    end

end
