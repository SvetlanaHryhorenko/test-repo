class BooksController < ApplicationController
before_action :set_book, only: [:show, :edit, :update, :destroy]

  def index
  # @publisher = Publisher.find(1)
#  @books = @publisher.books.all


   @authors=Author.all
   @books=[]
@authors=@authors.search(params[:a_search])
@authors.each do |author|
@books+= author.books.search(params[:search])
end



#@author=@authors.first

#@books = Book.all
#@books = @author.books
#@books = @books.all
#@books = @books.search(params[:search])
#@author=Author.search(params[:a_search])
#:a_id = "%#{@author.id}%"
#@books = @author.books.all
#@books = @books.a_search(5)
#@books = Book.all.where()
#@category=Category.find(params[:cat])
#@books = Book.includes(:categories).where(categories: { id: 5 })
#@category=Category.find(2) if params[:classic]
#@books = @books.category_search(params[:classic])#,params[:detective],params[:drama],params[:action],params[:science],params[:horror])
#@books = @books.search1(params[:search1])
#@message = "Hello"
  end
  
  def show
  end
  
  def new
    @book = Book.new
  end
  
  def edit
  end
  
  def create
    @book = Book.new(book_params)
  

    respond_to do |format|
      if @book.save
        format.html { redirect_to @book, notice: 'book was successfully created.' }
        format.json { render :show, status: :created, location: @book }
       # category = Category.find(params[:category_id])
       # book.categories << category
      else
        format.html { render :new }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    respond_to do |format|
      if @book.update(book_params)
        format.html { redirect_to @book, notice: 'book was successfully updated.' }
        format.json { render :show, status: :ok, location: @book }
        category = Category.find(params[:category_id])
        book.categories << category
      else
        format.html { render :edit }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @book.destroy
    respond_to do |format|
      format.html { redirect_to customers_url, notice: 'book was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book
      @book = Book.find(params[:id])
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def book_params
      params.require(:book).permit(:book_name, :rating, :publisher_id,:publishing_year, :description, :photo, :price, :quantity, :quantity, :author_id, :size, :category_id)
    end

end
