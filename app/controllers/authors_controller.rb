class AuthorsController < ApplicationController
before_action :set_book, only: [:show, :edit, :update, :destroy]

  def index
  # @publisher = Publisher.find(1)
#  @books = @publisher.books.all
    
@authors = Author.all

#@message = "Hello"
  end
  
  def show
  end
  
  def new
    @author = Author.new
  end
  
  def edit
  end
  
  def create
    @author = Author.new(author_params)
    respond_to do |format|
      if @author.save
        format.html { redirect_to @author, notice: 'author was successfully created.' }
        format.json { render :show, status: :created, location: @author }
      else
        format.html { render :new }
        format.json { render json: @author.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    respond_to do |format|
      if @author.update(author_params)
        format.html { redirect_to @author, notice: 'author was successfully updated.' }
        format.json { render :show, status: :ok, location: @author }
      else
        format.html { render :edit }
        format.json { render json: @author.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @book.destroy
    respond_to do |format|
      format.html { redirect_to authors_url, notice: 'author was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book
       @author = Author.find(params[:id])
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def author_params
      params.require(:author).permit(:author_name, :biography, :photo)
    end

end
