json.extract! publisher, :id, :publisher_name, :adress, :country, :phone_number :created_at, :updated_at
json.url publisher_url(publisher, format: :json)