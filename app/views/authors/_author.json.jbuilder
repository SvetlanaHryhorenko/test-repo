json.extract! author, :id, :author_name, :biography, :photo :created_at, :updated_at
json.url book_url(author, format: :json)