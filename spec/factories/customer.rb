FactoryGirl.define do
 factory :customer do
    sequence(:name) { |n| "name #{n}" }
    sequence(:email) { |n| "email #{n}" }
  end
end
