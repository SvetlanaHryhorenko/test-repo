require 'rails_helper'

describe CustomersController do
  describe 'GET :index' do
    let!(:customer) { create(:customer, name: 'Hello', em\: 'world') }

    it 'returns customers' do
      process :index, method: :get

      expect(assigns(:customers)).to eq [customer]
    end
  end

  describe 'CUSTOMER :create' do
    it 'creates customer' do
      process :create, method: :customer, params: {
        customer: {
          name: 'Hello',
          email: 'World'
        }
      }

      expect(Customer.count).to eq 1
      customer = Customer.first
      expect(customer.name).to eq 'Hello'
      expect(customer.email).to eq 'World'
    end
  end
end
