class CreateBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :books do |t|
      t.string :book_name
      t.integer :publishing_year
      t.float :rating
      t.text :description
      t.string :photo
      t.float :price
      t.integer :quantity
      t.integer :size
	   t.references :publisher , foreign_key: true
	   t.references :author , foreign_key: true

      t.timestamps
    end
  end
end
