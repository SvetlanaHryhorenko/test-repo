class CreateOrderItems < ActiveRecord::Migration[5.0]
  def change
    create_table :order_items do |t|
      t.integer :line_item
      t.integer :quantity
      t.integer :price
	   t.references :book , foreign_key: true

      t.timestamps
    end
  end
end
