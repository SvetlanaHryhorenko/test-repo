class CreateAuthorizationOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :authorization_orders do |t|
      t.date :delivery_date
      t.integer :order_price
	  t.references :customer , foreign_key: true

      t.timestamps
    end
  end
end
