class CreateAuthors < ActiveRecord::Migration[5.0]
  def change
    create_table :authors do |t|
      t.string :author_name
      t.text :biography
      t.string :photo

      t.timestamps
    end
  end
end
