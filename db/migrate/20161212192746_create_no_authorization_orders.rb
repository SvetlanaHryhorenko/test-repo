class CreateNoAuthorizationOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :no_authorization_orders do |t|
      t.date :delivery_date
      t.string :customer_name
      t.string :address
      t.integer :order_price
      t.string :email
      t.string :password
	  t.references :order_item , foreign_key: true

      t.timestamps
    end
  end
end
