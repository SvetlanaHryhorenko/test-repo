--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: author; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE author (
    id integer NOT NULL,
    author_name character varying(100),
    biography text,
    photo oid,
    created_at timestamp without time zone NOT NULL,
    update_at timestamp without time zone NOT NULL
);


--
-- Name: author_book; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE author_book (
    id integer NOT NULL,
    book_id integer NOT NULL,
    author_id integer NOT NULL
);


--
-- Name: authorization_order; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE authorization_order (
    id integer NOT NULL,
    customer_id integer,
    delivery_date date,
    order_price smallint
);


--
-- Name: authorization_orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE authorization_orders (
    id integer NOT NULL,
    delivery_date date,
    order_price integer,
    customer_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: authorization_orders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE authorization_orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: authorization_orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE authorization_orders_id_seq OWNED BY authorization_orders.id;


--
-- Name: authors; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE authors (
    id integer NOT NULL,
    author_name character varying,
    biography text,
    photo character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: authors_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE authors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: authors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE authors_id_seq OWNED BY authors.id;


--
-- Name: book; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE book (
    id integer NOT NULL,
    book_name character varying(100),
    publishing_year smallint,
    rating smallint,
    description text,
    photo oid,
    price smallint,
    quantity integer,
    size smallint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: books; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE books (
    id integer NOT NULL,
    book_name character varying,
    publishing_year integer,
    rating real,
    description text,
    photo character varying,
    price real,
    quantity integer,
    size integer,
    publisher_id integer,
    author_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: books_categories_test; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE books_categories_test (
    book_id integer,
    category_id integer
);


--
-- Name: books_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE books_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: books_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE books_id_seq OWNED BY books.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE categories (
    id integer NOT NULL,
    category_name character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: categories_books; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE categories_books (
    category_id integer,
    book_id integer
);


--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE categories_id_seq OWNED BY categories.id;


--
-- Name: categorizations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE categorizations (
    id integer NOT NULL,
    book_id integer,
    category_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: categorizations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE categorizations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: categorizations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE categorizations_id_seq OWNED BY categorizations.id;


--
-- Name: category; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE category (
    id integer NOT NULL,
    category_name character varying(50)
);


--
-- Name: category_book; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE category_book (
    id integer NOT NULL,
    book_id integer NOT NULL,
    category_id integer NOT NULL
);


--
-- Name: customer; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE customer (
    id integer NOT NULL,
    customer_name character varying(100),
    email character varying(25),
    phone_number character varying(20),
    address character varying(100)
);


--
-- Name: customers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE customers (
    id integer NOT NULL,
    customer_name character varying,
    email character varying,
    phone_number character varying,
    adress character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    password character varying
);


--
-- Name: customers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE customers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: customers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE customers_id_seq OWNED BY customers.id;


--
-- Name: no_authorization_order; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE no_authorization_order (
    id integer NOT NULL,
    delivery_date date,
    customer_name character varying(100),
    address character varying(100),
    order_price smallint,
    email character varying(25),
    password character varying(30)
);


--
-- Name: no_authorization_orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE no_authorization_orders (
    id integer NOT NULL,
    delivery_date date,
    customer_name character varying,
    address character varying,
    order_price integer,
    email character varying,
    password character varying,
    order_item_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: no_authorization_orders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE no_authorization_orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: no_authorization_orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE no_authorization_orders_id_seq OWNED BY no_authorization_orders.id;


--
-- Name: order_item; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE order_item (
    order_id integer NOT NULL,
    line_item integer NOT NULL,
    book_id integer NOT NULL,
    quantity integer,
    price smallint
);


--
-- Name: order_items; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE order_items (
    id integer NOT NULL,
    line_item integer,
    quantity integer,
    price integer,
    book_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: order_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE order_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: order_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE order_items_id_seq OWNED BY order_items.id;


--
-- Name: publisher; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE publisher (
    id integer NOT NULL,
    publisher_name character varying(100),
    address character varying(100),
    country character varying(25),
    phone_number character varying(20)
);


--
-- Name: publisher_book; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE publisher_book (
    id integer NOT NULL,
    book_id integer NOT NULL,
    publisher_id integer NOT NULL
);


--
-- Name: publishers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE publishers (
    id integer NOT NULL,
    publisher_name character varying,
    adress character varying,
    country character varying,
    phone_number character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: publishers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE publishers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: publishers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE publishers_id_seq OWNED BY publishers.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: authorization_orders id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY authorization_orders ALTER COLUMN id SET DEFAULT nextval('authorization_orders_id_seq'::regclass);


--
-- Name: authors id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY authors ALTER COLUMN id SET DEFAULT nextval('authors_id_seq'::regclass);


--
-- Name: books id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY books ALTER COLUMN id SET DEFAULT nextval('books_id_seq'::regclass);


--
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq'::regclass);


--
-- Name: categorizations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY categorizations ALTER COLUMN id SET DEFAULT nextval('categorizations_id_seq'::regclass);


--
-- Name: customers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY customers ALTER COLUMN id SET DEFAULT nextval('customers_id_seq'::regclass);


--
-- Name: no_authorization_orders id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY no_authorization_orders ALTER COLUMN id SET DEFAULT nextval('no_authorization_orders_id_seq'::regclass);


--
-- Name: order_items id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_items ALTER COLUMN id SET DEFAULT nextval('order_items_id_seq'::regclass);


--
-- Name: publishers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY publishers ALTER COLUMN id SET DEFAULT nextval('publishers_id_seq'::regclass);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: author_book author_book_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY author_book
    ADD CONSTRAINT author_book_pkey PRIMARY KEY (id);


--
-- Name: author author_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY author
    ADD CONSTRAINT author_pkey PRIMARY KEY (id);


--
-- Name: authorization_order authorization_order_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY authorization_order
    ADD CONSTRAINT authorization_order_pkey PRIMARY KEY (id);


--
-- Name: authorization_orders authorization_orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY authorization_orders
    ADD CONSTRAINT authorization_orders_pkey PRIMARY KEY (id);


--
-- Name: authors authors_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY authors
    ADD CONSTRAINT authors_pkey PRIMARY KEY (id);


--
-- Name: book book_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY book
    ADD CONSTRAINT book_pkey PRIMARY KEY (id);


--
-- Name: books books_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY books
    ADD CONSTRAINT books_pkey PRIMARY KEY (id);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: categorizations categorizations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY categorizations
    ADD CONSTRAINT categorizations_pkey PRIMARY KEY (id);


--
-- Name: category_book category_book_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY category_book
    ADD CONSTRAINT category_book_pkey PRIMARY KEY (id);


--
-- Name: category category_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id);


--
-- Name: customers customers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY customers
    ADD CONSTRAINT customers_pkey PRIMARY KEY (id);


--
-- Name: no_authorization_order no_authorization_order_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY no_authorization_order
    ADD CONSTRAINT no_authorization_order_pkey PRIMARY KEY (id);


--
-- Name: no_authorization_orders no_authorization_orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY no_authorization_orders
    ADD CONSTRAINT no_authorization_orders_pkey PRIMARY KEY (id);


--
-- Name: order_item order_item_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_item
    ADD CONSTRAINT order_item_pkey PRIMARY KEY (order_id, line_item);


--
-- Name: order_items order_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_items
    ADD CONSTRAINT order_items_pkey PRIMARY KEY (id);


--
-- Name: publisher_book publisher_book_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY publisher_book
    ADD CONSTRAINT publisher_book_pkey PRIMARY KEY (id);


--
-- Name: publisher publisher_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY publisher
    ADD CONSTRAINT publisher_pkey PRIMARY KEY (id);


--
-- Name: publishers publishers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY publishers
    ADD CONSTRAINT publishers_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: index_authorization_orders_on_customer_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_authorization_orders_on_customer_id ON authorization_orders USING btree (customer_id);


--
-- Name: index_books_on_author_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_books_on_author_id ON books USING btree (author_id);


--
-- Name: index_books_on_publisher_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_books_on_publisher_id ON books USING btree (publisher_id);


--
-- Name: index_categorizations_on_book_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_categorizations_on_book_id ON categorizations USING btree (book_id);


--
-- Name: index_categorizations_on_category_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_categorizations_on_category_id ON categorizations USING btree (category_id);


--
-- Name: index_no_authorization_orders_on_order_item_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_no_authorization_orders_on_order_item_id ON no_authorization_orders USING btree (order_item_id);


--
-- Name: index_order_items_on_book_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_order_items_on_book_id ON order_items USING btree (book_id);


--
-- Name: author_book author_book_author_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY author_book
    ADD CONSTRAINT author_book_author_id_fkey FOREIGN KEY (author_id) REFERENCES author(id);


--
-- Name: author_book author_book_book_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY author_book
    ADD CONSTRAINT author_book_book_id_fkey FOREIGN KEY (book_id) REFERENCES book(id);


--
-- Name: authorization_order authorization_order_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY authorization_order
    ADD CONSTRAINT authorization_order_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id);


--
-- Name: category_book category_book_book_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY category_book
    ADD CONSTRAINT category_book_book_id_fkey FOREIGN KEY (book_id) REFERENCES book(id);


--
-- Name: category_book category_book_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY category_book
    ADD CONSTRAINT category_book_category_id_fkey FOREIGN KEY (category_id) REFERENCES category(id);


--
-- Name: order_items fk_rails_24eeff6b07; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_items
    ADD CONSTRAINT fk_rails_24eeff6b07 FOREIGN KEY (book_id) REFERENCES books(id);


--
-- Name: no_authorization_orders fk_rails_37d59a3ba7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY no_authorization_orders
    ADD CONSTRAINT fk_rails_37d59a3ba7 FOREIGN KEY (order_item_id) REFERENCES order_items(id);


--
-- Name: categorizations fk_rails_494d06a06d; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY categorizations
    ADD CONSTRAINT fk_rails_494d06a06d FOREIGN KEY (book_id) REFERENCES books(id);


--
-- Name: books fk_rails_53d51ce16a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY books
    ADD CONSTRAINT fk_rails_53d51ce16a FOREIGN KEY (author_id) REFERENCES authors(id);


--
-- Name: categorizations fk_rails_5a40b79a1d; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY categorizations
    ADD CONSTRAINT fk_rails_5a40b79a1d FOREIGN KEY (category_id) REFERENCES categories(id);


--
-- Name: books fk_rails_d7ae2b039e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY books
    ADD CONSTRAINT fk_rails_d7ae2b039e FOREIGN KEY (publisher_id) REFERENCES publishers(id);


--
-- Name: authorization_orders fk_rails_e5a54ae2b1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY authorization_orders
    ADD CONSTRAINT fk_rails_e5a54ae2b1 FOREIGN KEY (customer_id) REFERENCES customers(id);


--
-- Name: order_item order_item_book_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_item
    ADD CONSTRAINT order_item_book_id_fkey FOREIGN KEY (book_id) REFERENCES book(id);


--
-- Name: publisher_book publisher_book_book_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY publisher_book
    ADD CONSTRAINT publisher_book_book_id_fkey FOREIGN KEY (book_id) REFERENCES book(id);


--
-- Name: publisher_book publisher_book_publisher_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY publisher_book
    ADD CONSTRAINT publisher_book_publisher_id_fkey FOREIGN KEY (publisher_id) REFERENCES publisher(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO schema_migrations (version) VALUES ('20161212181101'), ('20161212185545'), ('20161212190103'), ('20161212190253'), ('20161212191628'), ('20161212192414'), ('20161212192746'), ('20161212192952'), ('20161221224902'), ('20170123111454'), ('20170123112221'), ('20170123122139');


