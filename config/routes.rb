Rails.application.routes.draw do
  #get 'sessions/new'

  get 'authors/new'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #root to: 'home#index'
  resources :customers
  resources :books
  resources :publishers
  resources :authors
  resources :categories
  resources :categorizations
  resources :home

  get "log_out" => "sessions#destroy", :as => "log_out"
get "log_in" => "sessions#new", :as => "log_in"
get "sign_up" => "customers#new", :as => "sign_up"
#root :to => "customers#new"
resources :sessions

#root to: 'customers#new'
  #root to: 'home#index'
  root to: 'home#index'

end
